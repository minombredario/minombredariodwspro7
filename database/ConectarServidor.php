<?php

        
    class ConectarServidor{
        
        public static function conectar(){
            
           try{
               
               $bdconexion = new PDO('mysql:host=' . HOST, USUARIO, CONTRASEÑA);
               $bdconexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
               $bdconexion->exec("SET CHARACTER SET utf8");
              
               return $bdconexion;
               
            } catch (Exception $e) {

                die ('Error: ' . $e->getMessage());

                echo "la línea de error es: " . $e->getLine();//devuelve la linea donde esta el error
            }
        }
    }
    
  
?>

          